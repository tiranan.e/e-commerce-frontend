// Global SCSS can be placed only at this file
import '../assets/scss/app.scss';

// This default export is required in a new `pages/_app.js` file.
export default function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}