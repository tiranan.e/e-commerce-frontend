import * as React from "react";
import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import axios from "axios";

import styles from "../../assets/scss/pages/ProductItemPage.module.scss";

import Header from "../../components/Header";
import ContentBar from "../../components/ContentBar";
import ProductItem from "../../components/productItem";
import PriceChart from "../../components/PriceChart";
import SoldChart from "../../components/SoldChart";
import Duration from "../../components/Duration";
import AddProduct from "../../components/AddProduct";
import { alpha, styled } from "@mui/material/styles";
import { pink } from "@mui/material/colors";
import Switch from "@mui/material/Switch";
import ModalAddItem from "../../components/ModalAddItem";

const ProductID = ({ shopee, error }) => {
  console.log(shopee[0]);

  const [content, setContent] = useState("Items");

  const label = { inputProps: { "aria-label": "Switch demo" } };

  return (
    <div>
      <Header />
      <div className={styles.containerContentBar}>
        <div className={styles.topnav}>
          <div
            className={content === "Items" ? styles.active : ""}
            onClick={() => {
              setContent("Items");
            }}
          >
            Items
          </div>
          <div
            className={content === "Price" ? styles.active : ""}
            onClick={() => {
              setContent("Price");
            }}
          >
            Price History
          </div>
          <div
            className={content === "Sold" ? styles.active : ""}
            onClick={() => {
              setContent("Sold");
            }}
          >
            Sold History
          </div>
          <div
            className={content === "Alert" ? styles.active : ""}
            onClick={() => {
              setContent("Alert");
            }}
          >
            Alert
          </div>
        </div>
      </div>

      {content === "Items" && (
        <div className={styles.listProduct}>
          <ModalAddItem />
          {shopee.map((data, index) => (
            <div key={index}>
              {/* {console.log(data)} */}
              <ProductItem data={data} index={index} />
            </div>
          ))}
        </div>
      )}
      {content === "Price" && (
        <div>
          <div className={styles.durationContainer}>
            <Duration />
          </div>
          <div className={styles.chartContainer}>
            <PriceChart />
          </div>
        </div>
      )}
      {content === "Sold" && (
        <div>
          <div className={styles.durationContainer}>
            <Duration />
          </div>
          <div className={styles.chartContainer}>
            <SoldChart />
          </div>
        </div>
      )}
      {content === "Alert" && (
        <div className={styles.switchContainer}>
          <div className={styles.productNameLabel}>
            <div>Product Name</div>
            <input
              placeholder="Input Your Product Name"
              defaultValue="Apple iPhone"
            />
          </div>

          <div className={styles.alertLabel}>
            <Switch {...label} defaultChecked />
            <div>Alert when any item price below than </div>
            <input placeholder="Input Your Price" defaultValue="39,000" />
            <div>Bath.</div>
          </div>
          <div className={styles.alertLabel}>
            <Switch {...label} defaultChecked={false} />
            <div>
              Alert when any item price has discount since first record as
            </div>
            <input placeholder="Input Your Percentage" />
            <div>%. </div>
          </div>
          <div className={styles.alertLabel}>
            <Switch {...label} defaultChecked />
            <div>Alert when any item has</div>
            <input placeholder="Input Your Quantity" defaultValue="1" />
            <div> items left. </div>
          </div>
        </div>
      )}
    </div>
  );
};

ProductID.getInitialProps = async (ctx) => {
  try {
    const res = await axios.get("http://localhost:8000/shopee_items/");
    const shopee = res.data;
    return { shopee };
  } catch (error) {
    return { error };
  }
};

export default ProductID;
