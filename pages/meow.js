import fetch from "isomorphic-unfetch";

const Meow = ({ file }) => {
  return (
    <div>
      <img src={file} />
    </div>
  );
};

Meow.getInitialProps = async () => {
  const response = await fetch("https://aws.random.cat/meow");
  const data = await response.json();
  return data;
};

export default Meow;
