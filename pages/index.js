import * as React from "react";
import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import axios from "axios";

import Logo from "../assets/images/Logo.png";
import Link from "next/link";
import styles from "../assets/scss/pages/Index.module.scss";
import Header from "../components/Header";
import IconProduct from "../components/IconProduct";
import AddProduct from "../components/AddCategory";
import ModalAddCategory from "../components/ModalAddCategory";
import { faMobile, faSuitcase } from "@fortawesome/free-solid-svg-icons";
import FetchDataCategory from "../components/FetchDataCategory";

export default () => {
  const dataProduct1 = [{ name: "iPhone", icon: faMobile }];
  const dataProduct2 = [{ name: "Suitcase", icon: faSuitcase }];
  return (
    <>
      <Header />
      <div className={styles.text}>
        <div className={styles.subtitle}>YOU CAN ADD YOUR OWN CATEGORIES</div>
        <div className={styles.title}>Your Item Categories</div>
      </div>

      <FetchDataCategory/>

      <div className={styles.listProduct}>
        <ModalAddCategory />
        <Link href="/products/1">
          <a>
            <IconProduct data={dataProduct1} />
          </a>
        </Link>
        <IconProduct data={dataProduct2} />
      </div>
    </>
  );
};
