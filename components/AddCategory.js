import styles from "../assets/scss/components/IconProduct.module.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faMobile,
  faArrowCircleRight,
  faPlus,
} from "@fortawesome/free-solid-svg-icons";

const AddCategory = () => {
  return (
    <>
      <div className={styles.container}>
        <div className={styles.iconWarpper}>
          <div className={styles.icon}>
            <FontAwesomeIcon icon={faPlus} />
          </div>
        </div>
        <div className={styles.detail}>
          <div className={styles.title}>Add</div>
          <div className={styles.onClickItem}>
            <span>Category</span>
            <div className={styles.iconArrow}>
              <FontAwesomeIcon icon={faArrowCircleRight} />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default AddCategory;
