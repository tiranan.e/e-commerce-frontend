import { useState, useEffect } from "react";
// import Chart from "chart.js";
// import styles from "../assets/scss/components/PriceChart.module.scss";

import React from "react";
import Chart from "chart.js";
import styles from "../assets/scss/components/PriceChart.module.scss";

export default function CardLineChart() {
  useEffect(() => {
    const ctx = document.getElementById("myChart").getContext("2d");
    const myChart = new Chart(ctx, {
      type: "line",
      data: {
        labels: [
          "20 Mar",
          "21 Mar",
          "22 Mar",
          "23 Mar",
          "24 Mar",
          "25 Mar",
          "26 Mar",
          "27 Mar",
          "28 Mar",
          "29 Mar",
          "30 Mar",
          "31 Mar",
          "1 Apr",
          "2 Apr",
          "3 Apr",
          "4 Apr",
          "5 Apr",
          "6 Apr",
          "7 Apr",
          "8 Apr",
          "9 Apr",
          "10 Apr",
          "11 Apr",
          "12 Apr",
          "13 Apr",
          "14 Apr",
          "15 Apr",
          "16 Apr",
          "17 Apr",
          "18 Apr",
          "19 Apr",
          "20 Apr",
        ],
        datasets: [
          {
            label: "istudio_official_store",
            data: [39900,39900,39900,39900,39900,39900,39900,39900,39900,39900,39900,39900,39900,39900,39900,39900,39900,39900,39900,39900,39900,39900,39900,39900,39900,39900,39900,39900,39900,39900,39900,39900,39900],
            fill: false,
            borderColor: [
              // "rgba(255, 99, 132, 1)",
              "rgba(54, 162, 235, 1)",
              "rgba(255, 206, 86, 1)",
              "rgba(75, 192, 192, 1)",
              "rgba(153, 102, 255, 1)",
              "rgba(255, 159, 64, 1)",
            ],
            borderWidth: 4,
          },
          {
            label: "istudiobyspvi",
            data: [43900,43900,43900,43900,43900,43900,42900,42900,42900,42900,42900,42900,42900,42900,42900,42900,42900,42900,42900,42900,42900,42900,42900,42900,42900,42900,42900,42900,42900,42900,42900,42900,42900],
            fill: false,
            borderColor: [
              "rgba(255, 99, 132, 1)",
              "rgba(54, 162, 235, 1)",
              "rgba(255, 206, 86, 1)",
              "rgba(75, 192, 192, 1)",
              "rgba(153, 102, 255, 1)",
              "rgba(255, 159, 64, 1)",
            ],
            borderWidth: 4,
          },
          {
            label: "studio7online",
            data: [0,0,0,0,0,0,0,0,0,0,39900,39900,39900,39900,39900,39900,39900,39900,39900,39900,39900,39900,0,0,0,0,0,0,0,0,0,0,0],
            fill: false,
            borderColor: [
              "rgba(255, 206, 86, 1)",
              "rgba(75, 192, 192, 1)",
              "rgba(153, 102, 255, 1)",
            ],
            borderWidth: 4,
          },
          {
            label: "istudiobyuficon",
            data: [41900,41900,41900,41900,40000,39900,39900,39900,41900,41900,39900,39900,39900,39900,39900,39900,39900,41900,40000,39900,39900,39900,41900,41900,39900,39900,39900,39900,39900,39900,39900,39900],
            fill: false,
            borderColor: [
              // "rgba(255, 206, 86, 1)",
              "rgba(75, 192, 192, 1)",
              "rgba(153, 102, 255, 1)",
            ],
            borderWidth: 4,
          },
        ],
      },
      options: {
        scales: {
          y: {
            beginAtZero: true,
          },
        },
      },
    });
  }, []);
  return (
    <>
      <div className={styles.chart}>
        <canvas id="myChart"></canvas>
      </div>
    </>
  );
}
