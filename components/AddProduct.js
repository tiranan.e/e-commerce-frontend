import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    faPlus,
  } from "@fortawesome/free-solid-svg-icons";
// import AddURL from "./AddURL";
import styles from "../assets/scss/components/AddProduct.module.scss";

const AddProduct = () => {
  return (
    <>
      <div className={styles.cardContainer}>
          <div className={styles.iconWarpper}>
            <div className={styles.icon}>
              <FontAwesomeIcon icon={faPlus} />
            </div>
          </div>
          <div className={styles.description}>
              <div className={styles.item}>ITEMS</div>
              <div className={styles.add}>Add</div>
          </div>
      </div>
    </>
  );
};

export default AddProduct;
