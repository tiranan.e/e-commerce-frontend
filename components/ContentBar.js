import styles from "../assets/scss/components/ContentBar.module.scss";

const ContentBar = () => {
  return (
    <>
      <div className={styles.topnav}>
        <a className={styles.active} href="#home">
        Items
        </a>
        <a href="#news">Price History</a>
        <a href="#contact">Sold History</a>
        <a href="#about">Alert</a>
      </div>
    </>
  );
};

export default ContentBar;
