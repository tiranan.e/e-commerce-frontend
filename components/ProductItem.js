import Image from "next/image";
import Link from "next/link";
import ImageComponent from "./ImageComponent";
import styles from "../assets/scss/components/ProductItem.module.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStar } from "@fortawesome/free-solid-svg-icons";
import Badge from "./Badge";

const ProductItem = ({ data, index }) => {
  // console.log(data.variations[0][3].value);
  return (
    <>
      <div className={styles.cardContainer} key={index}>
        <div className={styles.imgItem}>
          <ImageComponent url={data.image} />
        </div>
        <div className={styles.detail}>
          <div className={styles.store}>
            {data.store_name} | rating {data.rating.avg_star}{" "}
          </div>

          <div className={styles.itemName}>
            <Link href={data.url}>
              <a>{data.name}</a>
            </Link>
          </div>
          <div className={styles.badgeOption}>
            <div>
              <Badge text={data.option1} />
            </div>
            <div>
              <Badge text={data.option2} />
            </div>
          </div>
          {data.variations[0][3].value === "null" ? (
            <div className={styles.price}>39900 ฿</div>
          ) : (
            <div className={styles.price}>{data.variations[0][3].value} ฿</div>
          )}

          {/* <div className={styles.price}>{data.variations[1][3].value} ฿</div> */}
        </div>
      </div>
    </>
  );
};

export default ProductItem;
