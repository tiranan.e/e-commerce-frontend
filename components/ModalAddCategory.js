import { useState } from "react";
import axios from "axios";
import { Modal, Button, Form } from "react-bootstrap";
import styles from "../assets/scss/components/ModalAddCategory.module.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faMobile,
  faArrowCircleRight,
  faPlus,
} from "@fortawesome/free-solid-svg-icons";

export default function ModalAddCategory() {
  const [show, setShow] = useState(false);
  const [dataPost, setDataPost] = useState([]);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [modifiedData, setModifiedData] = useState({
    name: "test",
  });

  const handleChange = (e) => {
    console.log(e.target.value);
    setModifiedData({
      name: e.target.value,
    });
  };

  const handleSubmit = async (e) => {
    console.log("handleSubmit");
    e.preventDefault();

    try {
      const response = await axios.post(
        "http://localhost:8000/product_categories/",
        modifiedData
      );
      console.log(response);
      setDataPost(response);
    } catch (error) {
      console.log("error");
    }
  };

  return (
    <>
      <div className={styles.container} onClick={handleShow}>
        <div className={styles.iconWarpper}>
          <div className={styles.icon}>
            <FontAwesomeIcon icon={faPlus} />
          </div>
        </div>
        <div className={styles.detail}>
          <div className={styles.title}>Add</div>
          <div className={styles.onClickItem}>
            <span>Category</span>
            <div className={styles.iconArrow}>
              <FontAwesomeIcon icon={faArrowCircleRight} />
            </div>
          </div>
        </div>
      </div>

      <Modal show={show} onHide={handleClose} centered={true}>
        <Modal.Header closeButton>
          <Modal.Title>Add Category</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form onSubmit={handleSubmit}>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <div className={styles.addCategory}>
                <div className={styles.addCategoryTitle}>
                  <label>Category name</label>
                </div>
                <div className={styles.addCategoryInput}>
                  <input
                    type="text"
                    name="name"
                    placeholder="ex. iPhone"
                    onChange={handleChange}
                  />
                </div>
                <div className={styles.addCategorySaveButton}>
                  <Button variant="primary" type="submit">
                    Save Change
                  </Button>
                </div>
              </div>
            </Form.Group>
          </form>
        </Modal.Body>
      </Modal>
    </>
  );
}
