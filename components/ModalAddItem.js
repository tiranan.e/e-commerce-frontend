import { useState } from "react";
import axios from "axios";
import { Modal, Button, Form } from "react-bootstrap";
import ButtonLoading from "../components/ButtonLoading";
import Badge from "./Badge";
import styles from "../assets/scss/components/ModalAddItem.module.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faMobile,
  faArrowCircleRight,
  faPlus,
} from "@fortawesome/free-solid-svg-icons";

export default function ModalAddItem() {
  const [show, setShow] = useState(false);
  const [indexOption, setIndexOption] = useState(null);
  const [indexOption2, setIndexOption2] = useState(null);
  const [dataPost, setDataPost] = useState([]);
  const [isPostUrl, setIsPostUrl] = useState(false);

  const handleClose = () => {
    setShow(false);
    setIsPostUrl(false);
  };

  const handleShow = () => setShow(true);

  const [modifiedData, setModifiedData] = useState({
    url: "",
  });

  const [modifiedDataFinal, setModifiedDataFinal] = useState({
    url: "",
    category: "iphone",
    option1: "",
    option2: "",
  });

  const handleChangeUrl = (e) => {
    console.log(e.target.value);
    setModifiedData({
      url: e.target.value,
    });
    setModifiedDataFinal((prev) => ({
      ...prev,
      url: e.target.value,
    }));
  };

  const handleChangeOption1 = (e) => {
    console.log("handleChangeOption1");
    console.log(e.target.value);
    setModifiedDataFinal({
      option1: "",
    });
  };

  const handleSubmit = async (e) => {
    // console.log("handleSubmit");
    e.preventDefault();

    try {
      const response = await axios.post(
        "http://localhost:8000/shopee_item_variations/",
        modifiedData
      );
      console.log(response);
      setDataPost(response);

      setIsPostUrl(true);
    } catch (error) {
      console.log("error");
      setIsPostUrl(false);
    }
  };

  const handleActiveBadge1 = (e, index) => {
    setIndexOption(index);
    // console.log(e.target.)
    // console.log(indexOption, "indexOption =");
  };

  const handleActiveBadge2 = (e, index) => {
    setIndexOption2(index);
    // console.log(indexOption2, "indexOption2 =");
  };

  const handleSaveChange = async (e) => {
    console.log("handleSaveChange");
    e.preventDefault();
    try {
      const response = await axios.post(
        "http://localhost:8000/shopee_item/",
        modifiedDataFinal
      );
      console.log(response);
      setShow(false);
      setIsPostUrl(false);
    } catch (error) {
      console.log("error");
    }
  };

  console.log(modifiedDataFinal);

  return (
    <>
      <div className={styles.cardContainer} onClick={handleShow}>
        <div className={styles.iconWarpper}>
          <div className={styles.icon}>
            <FontAwesomeIcon icon={faPlus} />
          </div>
        </div>
        <div className={styles.description}>
          <div className={styles.item}>ITEMS</div>
          <div className={styles.add}>Add</div>
        </div>
      </div>

      <Modal show={show} onHide={handleClose} centered={true}>
        <Modal.Header closeButton>
          <Modal.Title>ADD ITEM</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form onSubmit={handleSubmit}>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <div className={styles.urlInput}>
                <label>URL</label>
                <input
                  type="text"
                  name="name"
                  placeholder="https://shopee.co.th/example"
                  onChange={handleChangeUrl}
                />
                <Button variant="primary" type="submit">
                  <FontAwesomeIcon icon={faArrowCircleRight} color={"#fff"} />
                </Button>
              </div>
            </Form.Group>
          </form>

          {isPostUrl ? (
            <>
              <div className={styles.productName}>
                Product Name: {dataPost.data[0].name}
              </div>
              <div>
                <div className={styles.option1}>
                  <div className={styles.textOption}>
                    {dataPost.data[1].key}
                  </div>
                  <div className={styles.option}>
                    {dataPost.data[1].value.map((data, index) => (
                      <>
                        <div
                          className={
                            indexOption === index
                              ? styles.buttonContainerActive
                              : styles.buttonContainer
                          }
                          onClick={(e) => {
                            handleActiveBadge1(e, index);
                            console.log(data);
                            setModifiedDataFinal((prev) => ({
                              ...prev,
                              option1: data,
                            }));
                          }}
                          // onChange={handleChangeOption1}
                        >
                          {data}
                        </div>
                      </>
                    ))}
                  </div>
                </div>
              </div>
              <div>
                <div className={styles.option2}>
                  <div className={styles.textOption}>
                    {dataPost.data[2].key}
                  </div>
                  <div className={styles.option}>
                    {dataPost.data[2].value.map((data, index) => (
                      <>
                        <div
                          className={
                            indexOption2 === index
                              ? styles.buttonContainerActive
                              : styles.buttonContainer
                          }
                          onClick={(e) => {
                            handleActiveBadge2(e, index);
                            setModifiedDataFinal((prev) => ({
                              ...prev,
                              option2: data,
                            }));
                          }}
                        >
                          {data}
                        </div>
                      </>
                    ))}
                  </div>
                </div>
              </div>
            </>
          ) : (
            <>Plese the shopee link product</>
          )}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={handleSaveChange}>
            <div style={{ color: "#fff" }}>Save Changes</div>
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
