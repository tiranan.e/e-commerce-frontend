import styles from "../assets/scss/components/IconProduct.module.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faMobile,
  faArrowCircleRight,
  faSuitcase,
} from "@fortawesome/free-solid-svg-icons";

const IconProduct = ({ data }) => {
  return (
    <>
      <div className="row">
        {data && data.length
          ? data.map((source, index) => (
              <div className={styles.container}>
                <>
                  <div className={styles.iconWarpper} key={index}>
                    <div className={styles.icon}>
                      <FontAwesomeIcon icon={source.icon} />
                      {/* <FontAwesomeIcon icon={faMobile} /> */}
                    </div>
                  </div>
                  <div className={styles.detail}>
                    <div className={styles.title}>{source.name}</div>
                    <div className={styles.onClickItem}>
                      <span>Items</span>
                      <div className={styles.iconArrow}>
                        <FontAwesomeIcon icon={faArrowCircleRight} />
                      </div>
                    </div>
                  </div>
                </>
              </div>
            ))
          : ""}
      </div>
    </>
  );
};

export default IconProduct;
