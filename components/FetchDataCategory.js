import * as React from "react";
import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import axios from "axios";


const FetchDataCategory = ({ shopee, error }) => {
  console.log(shopee);

  const [content, setContent] = useState("Items");

  const label = { inputProps: { "aria-label": "Switch demo" } };

  return (
    <div>
      
    </div>
  );
};

FetchDataCategory.getInitialProps = async (ctx) => {
  try {
    const res = await axios.get("http://localhost:8000/shopee_items/");
    const shopee = res.data;
    return { shopee };
  } catch (error) {
    return { error };
  }
};

export default FetchDataCategory;
