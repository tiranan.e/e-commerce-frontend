import styles from "../assets/scss/components/Badge.module.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faMobile,
  faArrowCircleRight,
  faPlus,
} from "@fortawesome/free-solid-svg-icons";

const Badge = ({ text, isActive, onClick }) => {
  return (
    <>
    {/* className={content === "Alert" ? styles.active : ""} */}
      <div className={ isActive ? styles.badgeContainerActive : styles.badgeContainer}>
        <div className={styles.text}>{text}</div>
      </div>
    </>
  );
};

export default Badge;
