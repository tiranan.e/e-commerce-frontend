import Link from "next/link";
import Image from "next/image";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars, faBell, faHome } from "@fortawesome/free-solid-svg-icons";

// import Dropdown from "./Dropdown";
// import MenuNav from "./MenuNav";
import NotificationDropdown from "./NotificationDropdown";

import styles from "../assets/scss/components/Header.module.scss";

const Header = () => {
  return (
    <>
      <div className={styles.container}>
        {/* <Dropdown/> */}
        {/* <MenuNav /> */}
        <Link href="/">
          <a>
            <div className={styles.hamburger}>
              <FontAwesomeIcon icon={faHome} />
            </div>
          </a>
        </Link>
        <Link href="/">
          <a>
            <div className={styles.logoContainer}>
              <Image
                src="/static/images/Logo.png"
                alt="Logo"
                width={175}
                height={175}
              />
            </div>
          </a>
        </Link>
        <div className={styles.account}>
          <div className={styles.name}>ACCOUNT</div>
          <NotificationDropdown/>
        </div>
      </div>
    </>
  );
};

export default Header;
