import { Menu, Transition } from "@headlessui/react";
import { Fragment, useEffect, useRef, useState } from "react";
import { ChevronDownIcon } from "@heroicons/react/solid";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBell, faMobile } from "@fortawesome/free-solid-svg-icons";
import Link from "next/link";
import styles from "../assets/scss/components/NotificationDropdown.module.scss";

export default function NotificationDropdown() {
  return (
    <div className="w-56 text-right fixed top-16">
      <Menu as="div" className="relative inline-block text-left">
        <div className={styles.notification}>
          <Menu.Button className={styles.notificationButton}>
            <FontAwesomeIcon icon={faBell} color={"#e20f00"} />
          </Menu.Button>
        </div>
        <Transition
          as={Fragment}
          enter="transition ease-out duration-100"
          enterFrom="transform opacity-0 scale-95"
          enterTo="transform opacity-100 scale-100"
          leave="transition ease-in duration-75"
          leaveFrom="transform opacity-100 scale-100"
          leaveTo="transform opacity-0 scale-95"
        >
          <Menu.Items className={styles.menuItem}>
            <div className={styles.buttonItem}>
              <Link
                href={
                  "https://shopee.co.th/Apple-iPhone-13-Pro-%E0%B8%AB%E0%B8%99%E0%B9%89%E0%B8%B2%E0%B8%88%E0%B8%AD-6.1-%E0%B8%99%E0%B8%B4%E0%B9%89%E0%B8%A7-i.287137993.10451747711?sp_atk=03586e49-a671-4df6-b52d-6b671750a9ae"
                }
              >
                <a>
                  <Menu.Item>
                    <div className={styles.item}>
                      <div className={styles.iconNotification}>
                        <FontAwesomeIcon icon={faMobile} />
                      </div>
                      <div className={styles.textNotification}>
                        Apple iPhone 13 Pro Max 256GB has 1 item left
                      </div>
                    </div>
                  </Menu.Item>
                </a>
              </Link>
            </div>
            <div className={styles.buttonItem}>
              <Menu.Item>
                <div className={styles.item}>
                  <div className={styles.iconNotification}>
                    <FontAwesomeIcon icon={faMobile} />
                  </div>
                  <div className={styles.textNotification}>
                    Apple iPhone 13 Pro Max 256GB price has discount 10%.
                  </div>
                </div>
              </Menu.Item>
            </div>
            <div className={styles.buttonItem}>
              <Link
                href={
                  "https://shopee.co.th/Apple-iPhone-13-Pro-%E0%B8%AB%E0%B8%99%E0%B9%89%E0%B8%B2%E0%B8%88%E0%B8%AD-6.1-%E0%B8%99%E0%B8%B4%E0%B9%89%E0%B8%A7-i.287137993.10451747711?sp_atk=03586e49-a671-4df6-b52d-6b671750a9ae"
                }
              >
                <a>
                  <Menu.Item>
                    <div className={styles.item}>
                      <div className={styles.iconNotification}>
                        <FontAwesomeIcon icon={faMobile} />
                      </div>
                      <div className={styles.textNotification}>
                        Apple iPhone 13 Pro Max 256GB has 2 item left
                      </div>
                    </div>
                  </Menu.Item>
                </a>
              </Link>
            </div>
            <div className={styles.buttonItem}>
              <Menu.Item>
                <div className={styles.item}>
                  <div className={styles.iconNotification}>
                    <FontAwesomeIcon icon={faMobile} />
                  </div>
                  <div className={styles.textNotification}>
                    Apple iPhone 13 Pro Max 256GB below than 39,000 Bath.
                  </div>
                </div>
              </Menu.Item>
            </div>
            <div className={styles.buttonItem}>
              <Menu.Item>
                <div className={styles.item}>
                  <div className={styles.iconNotification}>
                    <FontAwesomeIcon icon={faMobile} />
                  </div>
                  <div className={styles.textNotification}>
                    Apple iPhone 13 Pro Max 256GB below than 39,500 Bath.
                  </div>
                </div>
              </Menu.Item>
            </div>
          </Menu.Items>
        </Transition>
      </Menu>
    </div>
  );
}

function EditInactiveIcon(props) {
  return (
    <svg
      {...props}
      viewBox="0 0 20 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M4 13V16H7L16 7L13 4L4 13Z"
        fill="#EDE9FE"
        stroke="#A78BFA"
        strokeWidth="2"
      />
    </svg>
  );
}

function EditActiveIcon(props) {
  return (
    <svg
      {...props}
      viewBox="0 0 20 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M4 13V16H7L16 7L13 4L4 13Z"
        fill="#8B5CF6"
        stroke="#C4B5FD"
        strokeWidth="2"
      />
    </svg>
  );
}

function DuplicateInactiveIcon(props) {
  return (
    <svg
      {...props}
      viewBox="0 0 20 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M4 4H12V12H4V4Z"
        fill="#EDE9FE"
        stroke="#A78BFA"
        strokeWidth="2"
      />
      <path
        d="M8 8H16V16H8V8Z"
        fill="#EDE9FE"
        stroke="#A78BFA"
        strokeWidth="2"
      />
    </svg>
  );
}

function DuplicateActiveIcon(props) {
  return (
    <svg
      {...props}
      viewBox="0 0 20 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M4 4H12V12H4V4Z"
        fill="#8B5CF6"
        stroke="#C4B5FD"
        strokeWidth="2"
      />
      <path
        d="M8 8H16V16H8V8Z"
        fill="#8B5CF6"
        stroke="#C4B5FD"
        strokeWidth="2"
      />
    </svg>
  );
}

function ArchiveInactiveIcon(props) {
  return (
    <svg
      {...props}
      viewBox="0 0 20 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <rect
        x="5"
        y="8"
        width="10"
        height="8"
        fill="#EDE9FE"
        stroke="#A78BFA"
        strokeWidth="2"
      />
      <rect
        x="4"
        y="4"
        width="12"
        height="4"
        fill="#EDE9FE"
        stroke="#A78BFA"
        strokeWidth="2"
      />
      <path d="M8 12H12" stroke="#A78BFA" strokeWidth="2" />
    </svg>
  );
}

function ArchiveActiveIcon(props) {
  return (
    <svg
      {...props}
      viewBox="0 0 20 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <rect
        x="5"
        y="8"
        width="10"
        height="8"
        fill="#8B5CF6"
        stroke="#C4B5FD"
        strokeWidth="2"
      />
      <rect
        x="4"
        y="4"
        width="12"
        height="4"
        fill="#8B5CF6"
        stroke="#C4B5FD"
        strokeWidth="2"
      />
      <path d="M8 12H12" stroke="#A78BFA" strokeWidth="2" />
    </svg>
  );
}

function MoveInactiveIcon(props) {
  return (
    <svg
      {...props}
      viewBox="0 0 20 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path d="M10 4H16V10" stroke="#A78BFA" strokeWidth="2" />
      <path d="M16 4L8 12" stroke="#A78BFA" strokeWidth="2" />
      <path d="M8 6H4V16H14V12" stroke="#A78BFA" strokeWidth="2" />
    </svg>
  );
}

function MoveActiveIcon(props) {
  return (
    <svg
      {...props}
      viewBox="0 0 20 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path d="M10 4H16V10" stroke="#C4B5FD" strokeWidth="2" />
      <path d="M16 4L8 12" stroke="#C4B5FD" strokeWidth="2" />
      <path d="M8 6H4V16H14V12" stroke="#C4B5FD" strokeWidth="2" />
    </svg>
  );
}

function DeleteInactiveIcon(props) {
  return (
    <svg
      {...props}
      viewBox="0 0 20 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <rect
        x="5"
        y="6"
        width="10"
        height="10"
        fill="#EDE9FE"
        stroke="#A78BFA"
        strokeWidth="2"
      />
      <path d="M3 6H17" stroke="#A78BFA" strokeWidth="2" />
      <path d="M8 6V4H12V6" stroke="#A78BFA" strokeWidth="2" />
    </svg>
  );
}

function DeleteActiveIcon(props) {
  return (
    <svg
      {...props}
      viewBox="0 0 20 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <rect
        x="5"
        y="6"
        width="10"
        height="10"
        fill="#8B5CF6"
        stroke="#C4B5FD"
        strokeWidth="2"
      />
      <path d="M3 6H17" stroke="#C4B5FD" strokeWidth="2" />
      <path d="M8 6V4H12V6" stroke="#C4B5FD" strokeWidth="2" />
    </svg>
  );
}
