import styles from "../assets/scss/components/Duration.module.scss";

const Duration = () => {
  return (
    <>
      <div className={styles.container}>
        <div className={styles.text}>Duration</div>
        <div className={styles.durationTag}>20 March 2022 - 20 April 2022</div>
      </div>
    </>
  );
};

export default Duration;
